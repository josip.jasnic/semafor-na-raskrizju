#include <Wire.h>
//
//#include <Arduino.h>
//#include <WiFi.h>
//#include <AsyncTCP.h>
//#include <ESPAsyncWebServer.h>
//#include <WebSerial.h>
//AsyncWebServer server(80);
//const char* ssid = "Redmi Note 8 Pro";          // Your WiFi SSID
//const char* password = "sifra123";  // Your WiFi Password

//expander 0x20 - hall senzori
//c
//adrese na expanderima 0x21 i 0x22
//crveno na glavnoj P0
//zuto na glavnoj P1
//zeleno na glavnoj P2
//
//crveno pjesacki preko glavne P3
//crveno pjesacki preko glavne P4
//
//zeleno pjeskacki preko glavne P5
//zeleno pjeskacki preko glavne P6
//
//crveno na sporednoj P10
//zuto na sporednoj P11
//zeleno na sporednoj P12
//
//crveno presacki preko sporedne P13
//crveno presacki preko sporedne P14
//
//zeleno pjesacki preko sporedne P15
//zeleno pjesacki preko sporedne P16

//c suprotan smjer je na P0 i P1
//c_count-- je na P0 i P1
//d suprotan smjer je na P16 i P17
//d_count-- je na P16 i P17

// Set I2C addresses
byte sensor_adress = 0x20;
byte traffic_light1 = 0x21;
byte traffic_light2 = 0x22;
// variables
int counter_old;
byte c; // first of the two bytes to read P0-P7, crna traka
byte c_direction = 0;
byte c_done = 0;
byte d; // second of the two bytes to read P10-P17
byte d_direction = 0;
byte d_done = 0;
byte input; // variable to receive the two bytes
const unsigned long eventInterval = 1000;
int c_activate = 0;
int counter;
int counter1 = 0;
int d_activate = 0;
int i;
int P0_P7[8];
int P0_P7_sensor[8];
int P0_P7_sensor_old[8];
int P10_P17[8];
int P10_P17_sensor[8];
int P10_P17_sensor_old[8];
int sum_c;
int sum_c_high;
int sum_c_in;
int sum_c_in_high;
int sum_c_in_low;
int sum_c_low;
int sum_d;
int sum_d_high;
int sum_d_low;
int sum_d_in;
int sum_d_in_high;
int sum_d_in_low;
bool switch_state_c = true;
bool switch_state_d = false;
int pushbutton_c = 25;
int pushbutton_d = 26;
bool pushed_c = false;
bool pushed_d = false;
bool remember_pushed_c = false;
bool remember_pushed_d = false;
unsigned long previousTime = 0;

void setup()
{
  // Serial Window (debugging)
  // za ESP32
  // Serial.begin(115200);
  // za arduino
  //  Serial.begin(9600);
  // I2C Two Wire initialisation
  Wire.begin();
  // PCF8574N is 'reverse' logic inasmuch it SINKS current
  // so HIGH is OFF and LOW is ON (will we remember this?)
  // Turn OFF all pins by sending a high byte (1 bit per byte)
  // by changing the 1's and 0's above you can control which pins are on and off.
  // Remember the chip can sink more than it can source So the 1's are off and the 0's are on.
  Serial.begin(115200);
  //    WiFi.mode(WIFI_STA);
  //    WiFi.begin(ssid, password);
  //    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
  //      Serial.printf("WiFi Failed!\n");
  //      return;
  //    }
  //    Serial.print("IP Address: ");
  //    Serial.println(WiFi.localIP());
  //    // WebSerial is accessible at "<IP Address>/webserial" in browser
  //    WebSerial.begin(&server);
  //    server.begin();
  pinMode(pushbutton_c, INPUT);
  pinMode(pushbutton_d, INPUT);
  Serial.println("Setup Complete.");
}

void loop()
{
  /* Updates frequently */
  unsigned long currentTime = millis();

  input = Wire.requestFrom(0x20, 2);
  c = Wire.read();
  d = Wire.read();
  Wire.endTransmission();
  pushed_c = digitalRead(pushbutton_c);
  pushed_d = digitalRead(pushbutton_d);


  /* Right shift num, n times and perform bitwise AND with 1 */
  for (i = 0; i < 8; i++)
  {
    P0_P7_sensor[i] = ~(c >> i) & 1;
    P10_P17_sensor[i] = ~(d >> i) & 1;
  }
  if ((P0_P7_sensor_old[4] == 1 && P0_P7_sensor[4] == 0) && sum_c_low == 0)
    sum_c_low = 1;
  if ((P0_P7_sensor_old[3] == 1 && P0_P7_sensor[3] == 0) && sum_c_low == 1)
    sum_c_low = 2;
  if ((P0_P7_sensor_old[2] == 1 && P0_P7_sensor[0] == 0) && sum_c_low == 2)
    sum_c_low = 3;
  if ((P0_P7_sensor_old[5] == 1 && P0_P7_sensor[5] == 0) && sum_c_high == 0)
    sum_c_high = 1;
  if ((P0_P7_sensor_old[6] == 1 && P0_P7_sensor[6] == 0) && sum_c_high == 1)
    sum_c_high = 2;
  if ((P0_P7_sensor_old[7] == 1 && P0_P7_sensor[7] == 0) && sum_c_high == 2)
    sum_c_high = 3;
  sum_c = sum_c_high + sum_c_low;

  if ((P10_P17_sensor_old[2] == 1 && P10_P17_sensor[2] == 0) && sum_d_low == 0)
    sum_d_low = 1;
  if ((P10_P17_sensor_old[1] == 1 && P10_P17_sensor[1] == 0) && sum_d_low == 1)
    sum_d_low = 2;
  if ((P10_P17_sensor_old[0] == 1 && P10_P17_sensor[0] == 0) && sum_d_low == 2)
    sum_d_low = 3;
  if ((P10_P17_sensor_old[4] == 1 && P10_P17_sensor[4] == 0) && sum_d_high == 0)
    sum_d_high = 1;
  // PINOVI 13 I 14 SU OBRNUTO SPOJENI!
  if ((P10_P17_sensor_old[3] == 1 && P10_P17_sensor[3] == 0) && sum_d_high == 1)
    sum_d_high = 2;
  if ((P10_P17_sensor_old[5] == 1 && P10_P17_sensor[5] == 0) && sum_d_high == 2)
    sum_d_high = 3;
  sum_d = sum_d_high + sum_d_low;

  if ((pushed_c == true) && (switch_state_d == false ))
    remember_pushed_c = true;
  if ((pushed_d == true) && (switch_state_c == false))
    remember_pushed_d = true;

  /* This is the event */
  if (currentTime - previousTime >= eventInterval) {
    /* Event code */
    counter++;
    if (counter > 35) {
      if ((sum_c >= 2 && sum_d_in <= 2 && switch_state_d == false) || (sum_d >= 2 && sum_c_in <= 2 && switch_state_c == false) || (remember_pushed_d == true && switch_state_c == false) || (remember_pushed_c == true && switch_state_d == false)  || (counter >= 150))
        if (currentTime - previousTime >= 500) {
          /* Event code */
          counter1++;
        }
    }
    //    /* Update the timing for the next time around */
    previousTime = currentTime;
  }



  if (counter < 4) // crveno svugdje
    pcf8575_write(word(B11100110, B11100110)); // crveno svugdje

  if (switch_state_c == false) {
    if (counter <= 5) {
      if (sum_c_low > 0)
        sum_c_in_low = 1;
      else
        sum_c_in_low = 0;
      if (sum_c_high > 0)
        sum_c_in_high = 1;
      else
        sum_c_in_high = 0;
    }

    if ((P0_P7_sensor_old[4] == 1 && P0_P7_sensor[4] == 0) && sum_c_low >= 1)
      sum_c_in_low++;
    if ((P0_P7_sensor_old[4] == 1 && P0_P7_sensor[4] == 0) && sum_c_high >= 1)
      sum_c_in_high++;
    sum_c_in = sum_c_in_high + sum_c_in_low;

    for (i = 0; i < 2; i++) {
      if ((P0_P7_sensor_old[i] == 1 && P0_P7_sensor[i]  == 0) || (P10_P17_sensor_old[i + 6] == 1 && P10_P17_sensor[i + 6] == 0)) {
        if (sum_c_in_low > 0)
          sum_c_in_low--;
        else
          sum_c_in_high--;
      }
    }
    if (sum_c_in_low < 0)
      sum_c_in_low = 0;
    if (sum_c_in_high < 0)
      sum_c_in_high = 0;

    if (counter > 5 && counter < 10)
      pcf8575_write(word(B11100110, B11100100)); // crveno na sporednoj, crveno i zuto na glavnoj, crveno za oba pjesacka
    if (counter > 10  && counter < 35)
      pcf8575_write(word(B10011110, B11100011)); // crveno na sporednoj, ZELENO na glavnoj, ZELENO za pjesake preko sporedne, crveno preko glavne
    if (counter1 > 0) {
      if (counter1 < 10) {
        if (counter1 % 2 == 0)
          pcf8575_write(word(B10011110, B11100011));
        else
          pcf8575_write(word( B11111110, B11100011));
      }
      //treperece zeleno za aute
      if (counter1 < 20 && counter1 >= 10) {
        if (counter1 % 2 == 0)
          pcf8575_write(word( B11100110, B11100111));
        else
          pcf8575_write(word( B11100110, B11100011));
      }
      //zuto
      if (counter1 >= 20 && counter1 < 30)
        pcf8575_write(word( B11100110, B11100101)); // crveno na sporednoj, zuto na glavnoj, crveno za oba pjesacka
      //crveno
      if (counter1 > 30 && counter1 < 32)
        pcf8575_write(word( B11100110, B11100110));
      if (counter1 >= 32) {
        sum_c_low = 0;
        sum_c_high = 0;
        sum_c_in_low = 0;
        sum_c_in_high = 0;
        counter = 0;
        counter1 = 0;
        switch_state_c = true;
        switch_state_d = false;
        remember_pushed_d = false;
        remember_pushed_c = false;
      }
    }
  }



  if (switch_state_d == false)  {
    if (counter <= 5) {
      if (sum_d_low > 0)
        sum_d_in_low = 1;
      else
        sum_d_in_low = 0;
      if (sum_d_high > 0)
        sum_d_in_high = 1;
      else
        sum_d_in_high = 0;
    }
    if ((P10_P17_sensor_old[2] == 1 && P10_P17_sensor[2] == 0) && sum_d_low >= 1)
      sum_d_in_low++;
    if ((P10_P17_sensor_old[4] == 1 && P10_P17_sensor[4] == 0) && sum_d_high >= 1)
      sum_d_in_high++;
    sum_d_in = sum_d_in_high + sum_d_in_low;

    for (i = 0; i < 2; i++) {
      if ((P0_P7_sensor_old[i] == 1 && P0_P7_sensor[i]  == 0) || (P10_P17_sensor_old[i + 6] == 1 && P10_P17_sensor[i + 6] == 0))
      {
        if (sum_d_in_low > 0)
          sum_d_in_low--;
        else
          sum_d_in_high--;

        if (sum_d_in_low < 0)
          sum_d_in_low = 0;
        if (sum_d_in_high < 0)
          sum_d_in_high = 0;
      }
    }
    if (counter > 5 && counter < 10)
      pcf8575_write(word(B11100100, B11100110)); // crveno na sporednoj, crveno i zuto na glavnoj, crveno za oba pjesacka
    if (counter > 10 && counter < 35)
      pcf8575_write(word(B11100011, B10011110)); // crveno na sporednoj, ZELENO na glavnoj, ZELENO za pjesake preko sporedne, crveno preko glavne
    if (counter1 > 0) {
      if (counter1 < 10) {
        if (counter1 % 2 == 0)
          pcf8575_write(word(B11100011, B10011110));
        else
          pcf8575_write(word(B11100011, B11111110));
      }
      //treperece zeleno za aute
      if (counter1 < 20 && counter1 >= 10) {
        if (counter1 % 2 == 0)
          pcf8575_write(word(B11100111, B11100110));
        else
          pcf8575_write(word(B11100011, B11100110));
      }
      //zuto
      if (counter1 >= 20 && counter1 < 30) {
        pcf8575_write(word(B11100101, B11100110)); // crveno na sporednoj, zuto na glavnoj, crveno za oba pjesacka
      }
      //crveno
      if (counter1 > 30 && counter1 < 32) {
        pcf8575_write(word(B11100110, B11100110));
      }
      if (counter1 >= 32) {
        {
          sum_d_low = 0;
          sum_d_high = 0;
          sum_d_in_low = 0;
          sum_d_in_high = 0;
          counter = 0;
          counter1 = 0;
          switch_state_c = false;
          switch_state_d  = true;
          remember_pushed_c = false;
          remember_pushed_d = false;
        }
      }
    }
  }



  if (counter - counter_old == 1) {
    Serial.print("C = ");
    Serial.print(c, BIN);
    Serial.print(" || D = ");
    Serial.println(d, BIN);
    Serial.print("counter: ");
    Serial.println(counter);
    Serial.print("counter1: ");
    Serial.println(counter1);
    Serial.print("sum_c: ");
    Serial.println(sum_c);
    Serial.print("sum_c_in: ");
    Serial.println(sum_c_in);
    Serial.print("sum_d: ");
    Serial.println(sum_d);
    Serial.print("sum_d_in: ");
    Serial.println(sum_d_in);
    Serial.print("switch c:");
    Serial.println(switch_state_c);
    Serial.print("switch d:");
    Serial.println(switch_state_d);
    Serial.print("c pushan: ");
    Serial.print(pushed_c);
    Serial.print(" || d pushan: ");
    Serial.println(pushed_d);

    //      WebSerial.print("counter: ");
    //      WebSerial.print(counter);
    //      WebSerial.print(" || counter1: ");
    //      WebSerial.println(counter1);
    //      WebSerial.print("sum_c: ");
    //      WebSerial.print(sum_c);
    //      WebSerial.print(" || sum_d: ");
    //      WebSerial.println(sum_d);
    //      WebSerial.print("sum_c_in: ");
    //      WebSerial.print(sum_c_in);
    //      WebSerial.print(" || sum_d_in: ");
    //      WebSerial.println(sum_d_in);
    //      WebSerial.print("c pushan: ");
    //      WebSerial.print(pushed_c);
    //      WebSerial.print(" || d pushan: ");
    //      WebSerial.println(pushed_d);
  } counter_old = counter;
  for (i = 0; i < 8; i++) {
    P0_P7_sensor_old[i] = P0_P7_sensor[i];
    P10_P17_sensor_old[i] = P10_P17_sensor[i];
  }
}      // send the data

void pcf8575_write(uint16_t data)
{
  // Function for writing two Bytes to the I2C expander device
  Wire.beginTransmission(0x021);
  Wire.write(lowByte(data));
  Wire.write(highByte(data));
  Wire.endTransmission();
  Wire.beginTransmission(0x022);
  Wire.write(lowByte(data));
  Wire.write(highByte(data));
  Wire.endTransmission();
}
//void serial_prints()
//{
//      Serial.print("P0_P7_sensor[i]: ");
//    for (i = 0; i < 8; i++)
//      Serial.print(P0_P7_sensor[i]);
//    Serial.println();
//    Serial.print("P0_P7_sensor_old[i]: ");
//    for (i = 0; i < 8; i++)
//      Serial.print(P0_P7_sensor_old[i]);
//    Serial.println();
//    Serial.print("P10_P17_sensor[i]: ");
//    for (i = 0; i < 8; i++)
//      Serial.print(P10_P17_sensor[i]);
//    Serial.println();
//    Serial.print("P10_P17_sensor_old[i]: ");
//    for (i = 0; i < 8; i++)
//      Serial.print(P10_P17_sensor_old[i]);
//  Serial.print("sum_c_low: ");
//  Serial.println(sum_c_low);
//  Serial.print("sum_c_high: ");
//  Serial.println(sum_c_high);
//  Serial.print("sum_d_low: ");
//  Serial.println(sum_d_low);
//  Serial.print("sum_d_high: ");
//  Serial.println(sum_d_high);
//  Serial.print("sum_c_in: ");
//  Serial.println(sum_c_in);
//  Serial.print("sum_d_in: ");
//  Serial.println(sum_d_in);

//
//

// if (counter - counter_old == 1) {
//    WebSerial.print("P0_P7_sensor[i]: ");
//    for (i = 0; i < 8; i++)
//      WebSerial.print(P0_P7_sensor[i]);
//    WebSerial.println();
//    WebSerial.print("P0_P7_sensor_old[i]: ");
//    for (i = 0; i < 8; i++)
//      WebSerial.print(P0_P7_sensor_old[i]);
//    WebSerial.println();
//    WebSerial.print("P10_P17_sensor[i]: ");
//    for (i = 0; i < 8; i++)
//      WebSerial.print(P10_P17_sensor[i]);
//    WebSerial.println();
//    WebSerial.print("P10_P17_sensor_old[i]: ");
//    for (i = 0; i < 8; i++)
//      WebSerial.print(P10_P17_sensor_old[i]);
// WebSerial.print(P10_P17_sensor[i]);
//    WebSerial.print("counter: ");
//    WebSerial.println(counter);
//    WebSerial.print("counter1: ");
//    WebSerial.println(counter1);
//    WebSerial.print("sum_c: ");
//    WebSerial.println(sum_c);
//    WebSerial.print("sum_d: ");
//    WebSerial.println(sum_d);
//    WebSerial.print("sum_c_in: ");
//    WebSerial.println(sum_c_in);
//    WebSerial.print("sum_d_in: ");
//    WebSerial.println(sum_d_in);
//    WebSerial.print("c pushan: ");
//    WebSerial.print(pushed_c);
//    WebSerial.print(" || d pushan: ");
//    WebSerial.println(pushed_d);
//    Serial.print("C = ");
//    Serial.print(c, BIN);
//    Serial.print(" || D = ");
//    Serial.println(d, BIN);
//    Serial.print("counter: ");
//    Serial.println(counter);
//    Serial.print("counter1: ");
//    Serial.println(counter1);
//    Serial.print("sum_c_low: ");
//    Serial.println(sum_c_low);
//    Serial.print("sum_c_high: ");
//    Serial.println(sum_c_high);
//    Serial.print("sum_c: ");
//    Serial.println(sum_c);
//    Serial.print("sum_d_low: ");
//    Serial.println(sum_d_low);
//    Serial.print("sum_d_high: ");
//    Serial.println(sum_d_high);
//    Serial.print("sum_d: ");
//    Serial.println(sum_d);
//    Serial.print("sum_c_in: ");
//    Serial.println(sum_c_in);
//    Serial.print("sum_d_in: ");
//    Serial.println(sum_d_in);
//    Serial.print("switch c:");
//    Serial.println(switch_state_c);
//    Serial.print("switch d:");
//    Serial.println(switch_state_d);
//    Serial.print("c pushan: ");
//    Serial.print(pushed_c);
//    Serial.print(" || d pushan: ");
//    Serial.println(pushed_d);
//
//  Serial.print("P0_P7_sensor[i]: ");
//  for (i = 0; i < 8; i++)
//    Serial.print(P0_P7_sensor[i]);
//  Serial.print(" || P0_P7_sensor_old[i]: ");
//  for (i = 0; i < 8; i++)
//    Serial.print(P0_P7_sensor_old[i]);
//  Serial.print(" || P10_P17_sensor[i]: ");
//  for (i = 0; i < 8; i++)
//    Serial.print(P10_P17_sensor[i]);
//  Serial.print(" || PP10_P17_sensor_old[i]: ");
//  for (i = 0; i < 8; i++)
//    Serial.print(P10_P17_sensor_old[i]);
//  Serial.print(" || Psum_c_low: ");
//  Serial.print(sum_c_low);
//  Serial.print(" || sum_c_high: ");
//  Serial.print(sum_c_high);
//     Serial.print(" || sum_c: ");
//  Serial.print(sum_c);
//  Serial.print(" || sum_d_low: ");
//  Serial.print(sum_d_low);
//  Serial.print(" || sum_d_high: ");
//  Serial.print(sum_d_high);
//     Serial.print(" || sum_d: ");
//  Serial.print(sum_d);
//  Serial.print("sum_c_in_low: ");
//  Serial.print(sum_c_in_low);
//  Serial.print(" || sum_c_in_high: ");
//  Serial.print(sum_c_in_high);
//     Serial.print(" || sum_c_in: ");
//  Serial.print(sum_c_in);
//  Serial.print(" || sum_d_in_low: ");
//  Serial.print(sum_d_in_low);
//  Serial.print(" || sum_d_in_high: ");
//  Serial.print(sum_d_in_high);
//  Serial.print(" ||  counter: ");
//  Serial.print(counter);
//  Serial.print(" || counter1: ");
//  Serial.print(counter1);
//  Serial.print("c pushan: ");
//  Serial.print(pushed_c);
//  Serial.print(" || d pushan: ");
//  Serial.println(pushed_d);
//  }  counter_old = counter; }
